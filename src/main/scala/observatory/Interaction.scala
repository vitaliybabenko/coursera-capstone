package observatory

import com.sksamuel.scrimage.{Image, Pixel, RGBColor}

import scala.math._

/**
  * 3rd milestone: interactive visualization
  */
object Interaction {

  /**
    * @param zoom Zoom level
    * @param x    X coordinate
    * @param y    Y coordinate
    * @return The latitude and longitude of the top-left corner of the tile, as per http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    */
  def tileLocation(zoom: Int, x: Int, y: Int): Location = Location(latitude(y, zoom), longtitude(x, zoom))


  /**
    * @param temperatures Known temperatures
    * @param colors       Color scale
    * @param zoom         Zoom level
    * @param x            X coordinate
    * @param y            Y coordinate
    * @return A 256×256 image showing the contents of the tile defined by `x`, `y` and `zooms`
    */
  def tile(temperatures: Iterable[(Location, Double)], colors: Iterable[(Double, Color)], zoom: Int, x: Int, y: Int): Image = {
    import Visualization._
    //
    val size = 256

    val points = (0 until size * size)
      .par.map(pos => {
      (longtitude((pos % size).toDouble / size + x, zoom), latitude((pos / size).toDouble / size + y, zoom))
    }).seq

    Image(size, size, getPixels(points, temperatures, colors, 127))
  }


  /**
    * Generates all the tiles for zoom levels 0 to 3 (included), for all the given years.
    *
    * @param yearlyData    Sequence of (year, data), where `data` is some data associated with
    *                      `year`. The type of `data` can be anything.
    * @param generateImage Function that generates an image given a year, a zoom level, the x and
    *                      y coordinates of the tile and the data to build the image from
    */
  def generateTiles[Data](yearlyData: Iterable[(Int, Data)],
                          generateImage: (Int, Int, Int, Int, Data) => Unit
                         ): Unit = {

    yearlyData.par.foreach { case (year, data) =>
      (0 to 3) foreach { zoom =>
        for {
          x <- 0 until math.pow(2, zoom).toInt
          y <- 0 until math.pow(2, zoom).toInt
        } generateImage(year, zoom, x, y, data)
      }
    }
  }

  private def longtitude(x: Double, z: Double): Double = x / pow(2, z) * 360 - 180

  private def latitude(y: Double, z: Double): Double = atan(sinh(Pi - y / pow(2, z) * 2 * Pi)) * 180 / Pi
}
