package observatory

import com.sksamuel.scrimage.{Image, Pixel}

/**
  * 2nd milestone: basic visualization
  */
object Visualization {

  private[this] val idwExponent = 3

  /**
    * @param temperatures Known temperatures: pairs containing a location and the temperature at this location
    * @param location     Location where to predict the temperature
    * @return The predicted temperature at `location`
    */
  def predictTemperature(temperatures: Iterable[(Location, Double)], location: Location): Double = {
    temperatures.find(_._1 == location) match {
      case Some(elem) => elem._2
      case None =>
        val nominator = temperatures.par.foldLeft(0.0) { case (acc, elem) => acc + elem._2 / distance(elem._1, location) }
        val denominator = temperatures.par.foldLeft(0.0) { case (acc, elem) => acc + 1 / distance(elem._1, location) }
        nominator / denominator
    }
  }

  /**
    * @param points Pairs containing a value and its associated color
    * @param value  The value to interpolate
    * @return The color that corresponds to `value`, according to the color scale defined by `points`
    */
  def interpolateColor(points: Iterable[(Double, Color)], value: Double): Color = {
    points find (_._1 == value) match {
      case Some(data) => data._2
      case None =>
        val (lower, upper) = points.toSeq sortBy (_._1) partition (_._1 < value)
        (lower.lastOption, upper.headOption) match {
          case (None, None) => Color(0, 0, 0)
          case (Some((_, c)), None) => c
          case (None, Some((_, c))) => c
          case (Some(pair1), Some(pair2)) => linearInterpolation(pair1, pair2, value)
        }
    }
  }

  /**
    * @param temperatures Known temperatures
    * @param colors       Color scale
    * @return A 360×180 image where each pixel shows the predicted temperature at its location
    */

  def visualize(temperatures: Iterable[(Location, Double)], colors: Iterable[(Double, Color)]): Image = {
    val width = 360
    val height = 180

    val points = for {
      y <- height / 2 until -height / 2 by -1
      x <- -width / 2 until width / 2
    } yield (x.toDouble, y.toDouble)

    Image(width, height, getPixels(points, temperatures, colors))
  }


  def getPixels(points: Seq[(Double, Double)], temperatures: Iterable[(Location, Double)], colors: Iterable[(Double, Color)], alpha: Int = 255): Array[Pixel] = {
    import observatory.SparkFactory.sparkContext

    val temperatureBroadcast = sparkContext.broadcast(temperatures)
    val colorsBroadcast = sparkContext.broadcast(colors)
    val rddPoints = sparkContext.parallelize(points)

    rddPoints
      .map { case (x, y) => predictTemperature(temperatureBroadcast.value, Location(y, x)) }
      .map(interpolateColor(colorsBroadcast.value, _))
      .map { case Color(red, green, blue) => Pixel(red, green, blue, alpha) }
      .collect()
  }

  def writeToPng(image: Image): Unit = image.output(new java.io.File("target/2000-temperature.png"))

  private def linearInterpolation(p1: (Double, Color), p2: (Double, Color), xP: Double): Color = {
    def lInter(y0: Int, y1: Int)(implicit xDelta: Double): Int = math.round(y0 + (y1 - y0) * xDelta).toInt

    implicit val xDelta = (xP - p1._1) / (p2._1 - p1._1)
    Color(lInter(p1._2.red, p2._2.red), lInter(p1._2.green, p2._2.green), lInter(p1._2.blue, p2._2.blue))
  }

  private def distance(knownPoint: Location, predictPoint: Location): Double = {
    import math._
    val R = 6372.8
    val dLat = (predictPoint.lat - knownPoint.lat).toRadians
    val dLon = (predictPoint.lon - knownPoint.lon).toRadians
    val a = pow(sin(dLat / 2), 2) + pow(sin(dLon / 2), 2) * cos(predictPoint.lat.toRadians) * cos(knownPoint.lat.toRadians)
    val c = 2 * asin(sqrt(a))
    pow(R * c, idwExponent)
  }
}

