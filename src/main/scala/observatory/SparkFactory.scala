package observatory

import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by vitaliy on 07.08.17.
  */
object SparkFactory {
  val sparkConf: SparkConf = new SparkConf().setMaster("local[*]").setAppName("DataExtractor")
  val sparkContext: SparkContext = new SparkContext(sparkConf)

}
