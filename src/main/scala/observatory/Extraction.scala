package observatory

import java.time.LocalDate

import com.sun.org.apache.bcel.internal.util.ClassLoader
import org.apache.spark.{SparkConf, SparkContext}

/**
  * 1st milestone: data extraction
  */
object Extraction {

  /**
    * @param year             Year number
    * @param stationsFile     Path of the stations resource file to use (e.g. "/stations.csv")
    * @param temperaturesFile Path of the temperatures resource file to use (e.g. "/1975.csv")
    * @return A sequence containing triplets (date, location, temperature)
    */
  def locateTemperatures(year: Int, stationsFile: String, temperaturesFile: String): Iterable[(LocalDate, Location, Double)] = {
    import observatory.SparkFactory.sparkContext

    val stRdd = sparkContext.textFile(getClass.getResource(stationsFile).getPath)
    val tmpRdd = sparkContext.textFile(getClass.getResource(temperaturesFile).getPath)
    val parsedStRdd = stRdd.filter(filterUndefinedStationLocation).map(parseStation)
    val parsedtmpRdd = tmpRdd.map(parseTemperature(_, year))
    parsedStRdd.join(parsedtmpRdd).map(prepareResult).collect().toIterable
  }

  private def filterUndefinedStationLocation(line: String): Boolean = {
    val parsedLine = line.split(",", -1)
    if (parsedLine(2).isEmpty || parsedLine(3).isEmpty) {
      false
    } else true
  }

  private def prepareResult(tuple: ((Option[Long], Option[Long]), (Station, Temperature))): (LocalDate, Location, Double) = {
    val temperature = tuple._2._2
    val station = tuple._2._1

    val data = LocalDate.of(temperature.year, temperature.month, temperature.day)
    val location = Location(station.latitude, station.longitude)
    val degree = 5.0 / 9 * (temperature.temperature - 32)
    (data, location, degree)
  }

  private def parseStation(line: String): ((Option[Long], Option[Long]), Station) = {
    val parsedLine = line.split(",", -1)
    val stId = if (parsedLine(0).isEmpty) None else Some(parsedLine(0).toLong)
    val stWbanId = if (parsedLine(1).isEmpty) None else Some(parsedLine(1).toLong)
    ((stId, stWbanId), Station(parsedLine(2).toDouble, parsedLine(3).toDouble))
  }

  private def parseTemperature(line: String, year: Int): ((Option[Long], Option[Long]), Temperature) = {
    val parsedLine = line.split(",", -1)
    val stId = if (parsedLine(0).isEmpty) None else Some(parsedLine(0).toLong)
    val stWbanId = if (parsedLine(1).isEmpty) None else Some(parsedLine(1).toLong)
    ((stId, stWbanId), Temperature(year.toInt, parsedLine(2).toInt, parsedLine(3).toInt, parsedLine(4).toDouble))
  }

  /**
    * @param records A sequence containing triplets (date, location, temperature)
    * @return A sequence containing, for each location, the average temperature over the year.
    */
  def locationYearlyAverageRecords(records: Iterable[(LocalDate, Location, Double)]): Iterable[(Location, Double)] = {
    records.groupBy(_._2).
      map {
        case (key, value) => (key, value.foldLeft(0.0)(_ + _._3) / value.size)
      }
  }
}

private case class Station(latitude: Double, longitude: Double)

private case class Temperature(year: Int, month: Int, day: Int, temperature: Double)
